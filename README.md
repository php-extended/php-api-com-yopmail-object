# php-extended/php-api-com-yopmail-object

A php API wrapper to connect to yopmail.com API

![coverage](https://gitlab.com/php-extended/php-api-com-yopmail-object/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-extended/php-api-com-yopmail-object/badges/master/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-extended/php-api-com-yopmail-object ^8`


## Basic Usage

For the basic functions, use :

```php

use PhpExtended\YopmailComApi\YopmailComApiEndpoint;
use PhpExtended\Endpoint\HttpEndpoint;

/** @var $client \Psr\Http\Client\ClientInterface */

$endpoint = new YopmailComApiEndpoint(new HttpEndpoint($client));

$endpoint->getEmailMetadatas('<username>', 1); // returns an array of YopmailComApiEmailMetadata
$endpoint->getEmail($metadata);	                // returns a YopmailComApiEmail 

```

Beware! Using this api too much results in being blocked with a google captcha
until resolved. Countermeasures are not studied in this library.


## License

MIT (See [license file](LICENSE)).
