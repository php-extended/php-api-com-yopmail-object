<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-com-yopmail-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiComYopmail;

use DateTimeInterface;
use PhpExtended\Email\EmailAddressInterface;
use PhpExtended\Email\MailboxListInterface;

/**
 * ApiComYopmailEmail class file.
 * 
 * This is a simple implementation of the ApiComYopmailEmailInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.ShortVariable")
 */
class ApiComYopmailEmail implements ApiComYopmailEmailInterface
{
	
	/**
	 * The target address email.
	 * 
	 * @var EmailAddressInterface
	 */
	protected EmailAddressInterface $_address;
	
	/**
	 * The id of this email.
	 * 
	 * @var string
	 */
	protected string $_id;
	
	/**
	 * The object of this email.
	 * 
	 * @var string
	 */
	protected string $_object;
	
	/**
	 * The address of the sender of this email.
	 * 
	 * @var MailboxListInterface
	 */
	protected MailboxListInterface $_from;
	
	/**
	 * The date of reception of this email.
	 * 
	 * @var DateTimeInterface
	 */
	protected DateTimeInterface $_dateReception;
	
	/**
	 * The full html contents of this email.
	 * 
	 * @var string
	 */
	protected string $_content;
	
	/**
	 * Constructor for ApiComYopmailEmail with private members.
	 * 
	 * @param EmailAddressInterface $address
	 * @param string $id
	 * @param string $object
	 * @param MailboxListInterface $from
	 * @param DateTimeInterface $dateReception
	 * @param string $content
	 */
	public function __construct(EmailAddressInterface $address, string $id, string $object, MailboxListInterface $from, DateTimeInterface $dateReception, string $content)
	{
		$this->setAddress($address);
		$this->setId($id);
		$this->setObject($object);
		$this->setFrom($from);
		$this->setDateReception($dateReception);
		$this->setContent($content);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the target address email.
	 * 
	 * @param EmailAddressInterface $address
	 * @return ApiComYopmailEmailInterface
	 */
	public function setAddress(EmailAddressInterface $address) : ApiComYopmailEmailInterface
	{
		$this->_address = $address;
		
		return $this;
	}
	
	/**
	 * Gets the target address email.
	 * 
	 * @return EmailAddressInterface
	 */
	public function getAddress() : EmailAddressInterface
	{
		return $this->_address;
	}
	
	/**
	 * Sets the id of this email.
	 * 
	 * @param string $id
	 * @return ApiComYopmailEmailInterface
	 */
	public function setId(string $id) : ApiComYopmailEmailInterface
	{
		$this->_id = $id;
		
		return $this;
	}
	
	/**
	 * Gets the id of this email.
	 * 
	 * @return string
	 */
	public function getId() : string
	{
		return $this->_id;
	}
	
	/**
	 * Sets the object of this email.
	 * 
	 * @param string $object
	 * @return ApiComYopmailEmailInterface
	 */
	public function setObject(string $object) : ApiComYopmailEmailInterface
	{
		$this->_object = $object;
		
		return $this;
	}
	
	/**
	 * Gets the object of this email.
	 * 
	 * @return string
	 */
	public function getObject() : string
	{
		return $this->_object;
	}
	
	/**
	 * Sets the address of the sender of this email.
	 * 
	 * @param MailboxListInterface $from
	 * @return ApiComYopmailEmailInterface
	 */
	public function setFrom(MailboxListInterface $from) : ApiComYopmailEmailInterface
	{
		$this->_from = $from;
		
		return $this;
	}
	
	/**
	 * Gets the address of the sender of this email.
	 * 
	 * @return MailboxListInterface
	 */
	public function getFrom() : MailboxListInterface
	{
		return $this->_from;
	}
	
	/**
	 * Sets the date of reception of this email.
	 * 
	 * @param DateTimeInterface $dateReception
	 * @return ApiComYopmailEmailInterface
	 */
	public function setDateReception(DateTimeInterface $dateReception) : ApiComYopmailEmailInterface
	{
		$this->_dateReception = $dateReception;
		
		return $this;
	}
	
	/**
	 * Gets the date of reception of this email.
	 * 
	 * @return DateTimeInterface
	 */
	public function getDateReception() : DateTimeInterface
	{
		return $this->_dateReception;
	}
	
	/**
	 * Sets the full html contents of this email.
	 * 
	 * @param string $content
	 * @return ApiComYopmailEmailInterface
	 */
	public function setContent(string $content) : ApiComYopmailEmailInterface
	{
		$this->_content = $content;
		
		return $this;
	}
	
	/**
	 * Gets the full html contents of this email.
	 * 
	 * @return string
	 */
	public function getContent() : string
	{
		return $this->_content;
	}
	
}
