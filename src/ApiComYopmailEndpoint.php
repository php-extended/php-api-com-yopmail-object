<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-com-yopmail-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiComYopmail;

use ArrayIterator;
use DateTimeImmutable;
use InvalidArgumentException;
use PhpExtended\Email\EmailAddressParser;
use PhpExtended\Email\EmailAddressParserInterface;
use PhpExtended\Email\MailboxListParser;
use PhpExtended\Email\MailboxListParserInterface;
use PhpExtended\Email\MailboxParser;
use PhpExtended\Html\HtmlCollectionNodeInterface;
use PhpExtended\Html\HtmlParser;
use PhpExtended\Html\HtmlParserInterface;
use PhpExtended\HttpMessage\RequestFactory;
use PhpExtended\HttpMessage\UriFactory;
use PhpExtended\Iterator\PagedIterator;
use PhpExtended\Iterator\PagedIteratorInterface;
use PhpExtended\Parser\ParseThrowable;
use Psr\Http\Client\ClientExceptionInterface;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\UriFactoryInterface;
use RuntimeException;

/**
 * ApiComYopmailEndpoint class file.
 * 
 * This class represents the endpoint for all yopmail accounts.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.CouplingBetweenObjects")
 */
class ApiComYopmailEndpoint implements ApiComYopmailEndpointInterface
{
	
	/**
	 * The hostname of yopmail.
	 * 
	 * @var string
	 */
	public const HOST = 'http://yopmail.com';
	
	/**
	 * The http client.
	 * 
	 * @var ClientInterface
	 */
	protected ClientInterface $_httpClient;
	
	/**
	 * The uri factory.
	 * 
	 * @var UriFactoryInterface
	 */
	protected UriFactoryInterface $_uriFactory;
	
	/**
	 * The request factory.
	 * 
	 * @var RequestFactoryInterface
	 */
	protected RequestFactoryInterface $_requestFactory;
	
	/**
	 * The html parser.
	 * 
	 * @var HtmlParserInterface
	 */
	protected HtmlParserInterface $_htmlParser;
	
	/**
	 * The email parser.
	 * 
	 * @var EmailAddressParserInterface
	 */
	protected EmailAddressParserInterface $_emailParser;
	
	/**
	 * The mailbox list parser.
	 * 
	 * @var MailboxListParserInterface
	 */
	protected MailboxListParserInterface $_mailboxListParser;
	
	/**
	 * The current version of the api runned by yopmail.
	 * 
	 * @var ?string
	 */
	protected ?string $_version = null;
	
	/**
	 * Builds a new endpoint with its dependancies.
	 * 
	 * @param ClientInterface $client
	 * @param ?UriFactoryInterface $uriFactory
	 * @param ?RequestFactoryInterface $requestFactory
	 * @param ?HtmlParserInterface $htmlParser
	 * @param ?EmailAddressParserInterface $emailAddressParser
	 * @param ?MailboxListParserInterface $mailboxListParser
	 */
	public function __construct(
		ClientInterface $client,
		?UriFactoryInterface $uriFactory = null,
		?RequestFactoryInterface $requestFactory = null,
		?HtmlParserInterface $htmlParser = null,
		?EmailAddressParserInterface $emailAddressParser = null,
		?MailboxListParserInterface $mailboxListParser = null
	) {
		$this->_httpClient = $client;
		$this->_uriFactory = $uriFactory ?? new UriFactory();
		$this->_requestFactory = $requestFactory ?? new RequestFactory();
		$this->_htmlParser = $htmlParser ?? new HtmlParser();
		$this->_emailParser = $emailAddressParser ?? new EmailAddressParser();
		$this->_mailboxListParser = $mailboxListParser ?? new MailboxListParser(new MailboxParser($this->_emailParser));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiComYopmail\ApiComYopmailEndpointInterface::getEmailMetadatas()
	 * @throws ClientExceptionInterface
	 * @throws InvalidArgumentException
	 * @throws ParseThrowable
	 * @throws RuntimeException
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 * @SuppressWarnings("PHPMD.ExcessiveMethodLength")
	 */
	public function getEmailMetadatas(string $username, int $page = 1) : PagedIteratorInterface
	{
		$uri = $this->_uriFactory->createUri(self::HOST.'/inbox.php?login='.\urlencode($username).'&p='.((string) $page).'&v='.$this->getVersion());
		$request = $this->_requestFactory->createRequest('GET', $uri);
		$response = $this->_httpClient->sendRequest($request);
		$dom = $this->_htmlParser->parse($response->getBody()->__toString());
		
		$fullData = $dom->getText();
		if(false !== \mb_stripos($fullData, 'No mail for'))
		{
			return new PagedIterator(new ArrayIterator(), 1, 1);
		}
		
		$maxpage = 1;
		$maxpagea = $dom->findNodeCss('a.next');
		if(null === $maxpagea)
		{
			$message = 'Failed to find max page in data at {url}';
			$context = ['{url}' => $uri->__toString()];
			
			throw new RuntimeException(\strtr($message, $context));
		}
		
		$maxpageat = $maxpagea->getAttribute('title');
		if(null === $maxpageat)
		{
			$message = 'Failed to find title attribute in data at {url}';
			$context = ['{url}' => $uri->__toString()];
			
			throw new RuntimeException(\strtr($message, $context));
		}
		
		$matches = [];
		$res = \preg_match('#(\\d+)/(\\d+)#', $maxpageat->getValue(), $matches);
		if(false === $res || !isset($matches[1]) || !isset($matches[2]))
		{
			$message = 'Failed to parse max page in data at {url}';
			$context = ['{url}' => $uri->__toString()];
			
			throw new RuntimeException(\strtr($message, $context));
		}
		
		$maxpage = \max($maxpage, (int) $matches[1]);
		
		/** @var array<integer, ApiComYopmailEmailMetadataInterface> $messages */
		$messages = [];
		
		foreach($dom->findAllNodesCss('div.m') as $k => $messageDiv)
		{
			/** @var HtmlCollectionNodeInterface $messageDiv */
			$anchor = $messageDiv->findNodeCss('a.lm');
			if(null === $anchor)
			{
				$message = 'Failed to find anchor div at the {k}th row at url {url}';
				$context = ['{k}' => $k, '{url}' => $uri->__toString()];
				
				throw new RuntimeException(\strtr($message, $context));
			}
			
			$anchorhref = $anchor->getAttribute('href');
			if(null === $anchorhref)
			{
				$message = 'Failed to find anchor href at the {k}th row at url {url}';
				$context = ['{k}' => $k, '{url}' => $uri->__toString()];
				
				throw new RuntimeException(\strtr($message, $context));
			}
			
			$timespan = $messageDiv->findNodeCss('span.lmh');
			if(null === $timespan)
			{
				$message = 'Failed to find time span at the {k}th row at url {url}';
				$context = ['{k}' => $k, '{url}' => $uri->__toString()];
				
				throw new RuntimeException(\strtr($message, $context));
			}
			
			$subjectspan = $messageDiv->findNodeCss('span.lmf');
			if(null === $subjectspan)
			{
				$message = 'Failed to find subject span at the {k}th row at url {url}';
				$context = ['{k}' => $k, '{url}' => $uri->__toString()];
				
				throw new RuntimeException(\strtr($message, $context));
			}
			
			$linespan = $messageDiv->findNodeCss('span.lms');
			if(null === $linespan)
			{
				$message = 'Failed to find line span at the {k}th row at url {url}';
				$context = ['{k}' => $k, '{url}' => $uri->__toString()];
				
				throw new RuntimeException(\strtr($message, $context));
			}
			
			$matches = [];
			$res = \preg_match('#b=(.*)&id=(.*)$#', $anchorhref->getValue(), $matches);
			if(false === $res || !isset($matches[1]) || !isset($matches[2]))
			{
				$message = 'Failed to parse mail id at the {k}th row at url {url}';
				$context = ['{k}' => $k, '{url}' => $uri->__toString()];
				
				throw new RuntimeException(\strtr($message, $context));
			}
			
			if($matches[1] !== $username)
			{
				$message = 'Failed to confirm user email at the {k}th row at url {url}';
				$context = ['{k}' => $k, '{url}' => $uri->__toString()];
				
				throw new RuntimeException(\strtr($message, $context));
			}
			
			$mailid = $matches[2];
			
			$object = $subjectspan->getText();
			
			$dateReception = DateTimeImmutable::createFromFormat('G:i', $timespan->getText());
			if(false === $dateReception)
			{
				$message = 'Failed to parse reception data at the {k}th row at url {url}';
				$context = ['{k}' => $k, '{url}' => $uri->__toString()];
				
				throw new RuntimeException(\strtr($message, $context));
			}
			
			$headline = $linespan->getText();
			
			$metadata = new ApiComYopmailEmailMetadata(
				$this->_emailParser->parse($username),
				$mailid,
				$object,
				$dateReception,
				$headline,
			);
			$messages[] = $metadata;
		}
		
		return new PagedIterator(new ArrayIterator($messages), $page, $maxpage);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiComYopmail\ApiComYopmailEndpointInterface::getEmail()
	 * @throws ClientExceptionInterface
	 * @throws InvalidArgumentException
	 * @throws ParseThrowable
	 * @throws RuntimeException
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 * @SuppressWarnings("PHPMD.ExcessiveMethodLength")
	 */
	public function getEmail(ApiComYopmailEmailMetadataInterface $metadata) : ApiComYopmailEmailInterface
	{
		$uri = $this->_uriFactory->createUri(self::HOST.'/m.php?b='.\urlencode($metadata->getAddress()->__toString()).'&id='.\urlencode($metadata->getId()));
		$request = $this->_requestFactory->createRequest('GET', $uri);
		$response = $this->_httpClient->sendRequest($request);
		$dom = $this->_htmlParser->parse($response->getBody()->__toString());
		
		// metadata first
		/** @var ?\PhpExtended\Html\HtmlAbstractNodeInterface $mailhaut */
		$mailhaut = $dom->findNodeCss('div#mailhaut');
		if(null === $mailhaut || !($mailhaut instanceof HtmlCollectionNodeInterface))
		{
			$message = 'Failed to find mail headers at url {url}';
			$context = ['{url}' => $uri->__toString()];
			
			throw new RuntimeException(\strtr($message, $context));
		}
		
		$object = null;
		$from = null;
		$dateReception = null;
		
		foreach($mailhaut->getChildren() as $k => $childNode)
		{
			/** @var HtmlCollectionNodeInterface $childNode */
			if('div' !== $childNode->getName())
			{
				continue;
			}
			
			$attribute = $childNode->getAttribute('class');
			if(null !== $attribute && 'f16' === $attribute->getName())
			{
				$object = $childNode->getText();
				continue;
			}
			
			if(!$childNode->hasAttribute('class') || empty($childNode->getAttribute('class')))
			{
				$bnode = $childNode->findNodeCss('b');
				if(null === $bnode)
				{
					$message = 'Failed to interpret {k}th header at url {url}';
					$context = ['{k}' => $k, '{url}' => $uri->__toString()];
					
					throw new RuntimeException(\strtr($message, $context));
				}
				
				$btext = $bnode->getText();
				
				switch($btext)
				{
					case 'From:':
						$from = \trim(\str_replace('From:', '', $childNode->getText()));
						break;
					
					case 'Date:':
						$time = \trim(\trim(\str_replace('Date:', '', $childNode->getText())), \html_entity_decode('&nbsp;'));
						$dateReception = DateTimeImmutable::createFromFormat('Y-m-d H:i', $time);
						if(false === $dateReception)
						{
							$message = 'Failed to parse date reception at url {url}';
							$context = ['{url}' => $uri->__toString()];
							
							throw new RuntimeException(\strtr($message, $context));
						}
						break;
					
					default:
						// ignore
						break;
				}
			}
		}
		if(null === $object)
		{
			$message = 'Failed to find object at url {url}';
			$context = ['{url}' => $uri->__toString()];
			
			throw new RuntimeException(\strtr($message, $context));
		}
		
		if(null === $from)
		{
			$message = 'Failed to find FROM at url {url}';
			$context = ['{url}' => $uri->__toString()];
			
			throw new RuntimeException(\strtr($message, $context));
		}
		
		if(null === $dateReception)
		{
			$message = 'Failed to find DATE at url {url}';
			$context = ['{url}' => $uri->__toString()];
			
			throw new RuntimeException(\strtr($message, $context));
		}
		
		// real data
		$mailmilieu = $dom->findNodeCss('div#mailmillieu');
		if(null === $mailmilieu)
		{
			$message = 'Failed to find mail milieu at url {url}';
			$context = ['{url}' => $uri->__toString()];
			
			throw new RuntimeException(\strtr($message, $context));
		}
		
		/*
<div id="mailmillieu">
	<div> 									# find('div', 0)
		<div style="padding:8px"></div>		# find('div', 1)
	</div>
	<div>	# <<== capture this div !		# find('div', 2)
		// real content
	</div>
</div>
		 */
		
		$divmail = $mailmilieu->findNodeCss('div', 2);
		if(null === $divmail)
		{
			$message = 'Failed to find mail milieu mail div at url {url}';
			$context = ['{url}' => $uri->__toString()];
			
			throw new RuntimeException(\strtr($message, $context));
		}
		
		$content = $divmail->getText();
		if(64 > \mb_strlen($content))	// arbitrary length
		{
			$message = 'Failed to parse mail milieu div at url {url}';
			$context = ['{url}' => $uri->__toString()];
			
			throw new RuntimeException(\strtr($message, $context));
		}
		
		return new ApiComYopmailEmail(
			$metadata->getAddress(),
			$metadata->getId(),
			$metadata->getObject(),
			$this->_mailboxListParser->parse($from),
			$dateReception,
			$content,
		);
	}
	
	/**
	 * Gets the version number from the yopmail api.
	 * 
	 * @return string
	 * @throws ClientExceptionInterface
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 */
	protected function getVersion() : string
	{
		if(null === $this->_version)
		{
			$uri = $this->_uriFactory->createUri(self::HOST.'/en/');
			$request = $this->_requestFactory->createRequest('GET', $uri);
			$response = $this->_httpClient->sendRequest($request);
			$data = $response->getBody()->__toString();
			
			$data = \str_replace(["\r", "\n"], '', $data);	// make inline
			
			$matches = [];
			$res = \preg_match('#/style/(\\d\\.\\d)/style\\.css#', $data, $matches);
			if(false === $res || !isset($matches[1]))
			{
				$message = 'Failed to find version number in url {url}';
				$context = ['{url}' => self::HOST.'/en/'];
				
				throw new RuntimeException(\strtr($message, $context));
			}
			
			$this->_version = $matches[1];
		}
		
		return $this->_version;
	}
	
}
