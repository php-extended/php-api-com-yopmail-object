<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-com-yopmail-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiComYopmail;

use DateTimeInterface;
use PhpExtended\Email\EmailAddressInterface;

/**
 * ApiComYopmailEmailMetadata class file.
 * 
 * This is a simple implementation of the ApiComYopmailEmailMetadataInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.ShortVariable")
 */
class ApiComYopmailEmailMetadata implements ApiComYopmailEmailMetadataInterface
{
	
	/**
	 * The target address email.
	 * 
	 * @var EmailAddressInterface
	 */
	protected EmailAddressInterface $_address;
	
	/**
	 * The id of this email.
	 * 
	 * @var string
	 */
	protected string $_id;
	
	/**
	 * The object of this email.
	 * 
	 * @var string
	 */
	protected string $_object;
	
	/**
	 * The date of reception of this email.
	 * 
	 * @var DateTimeInterface
	 */
	protected DateTimeInterface $_dateReception;
	
	/**
	 * The headline of the email.
	 * 
	 * @var string
	 */
	protected string $_headline;
	
	/**
	 * Constructor for ApiComYopmailEmailMetadata with private members.
	 * 
	 * @param EmailAddressInterface $address
	 * @param string $id
	 * @param string $object
	 * @param DateTimeInterface $dateReception
	 * @param string $headline
	 */
	public function __construct(EmailAddressInterface $address, string $id, string $object, DateTimeInterface $dateReception, string $headline)
	{
		$this->setAddress($address);
		$this->setId($id);
		$this->setObject($object);
		$this->setDateReception($dateReception);
		$this->setHeadline($headline);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the target address email.
	 * 
	 * @param EmailAddressInterface $address
	 * @return ApiComYopmailEmailMetadataInterface
	 */
	public function setAddress(EmailAddressInterface $address) : ApiComYopmailEmailMetadataInterface
	{
		$this->_address = $address;
		
		return $this;
	}
	
	/**
	 * Gets the target address email.
	 * 
	 * @return EmailAddressInterface
	 */
	public function getAddress() : EmailAddressInterface
	{
		return $this->_address;
	}
	
	/**
	 * Sets the id of this email.
	 * 
	 * @param string $id
	 * @return ApiComYopmailEmailMetadataInterface
	 */
	public function setId(string $id) : ApiComYopmailEmailMetadataInterface
	{
		$this->_id = $id;
		
		return $this;
	}
	
	/**
	 * Gets the id of this email.
	 * 
	 * @return string
	 */
	public function getId() : string
	{
		return $this->_id;
	}
	
	/**
	 * Sets the object of this email.
	 * 
	 * @param string $object
	 * @return ApiComYopmailEmailMetadataInterface
	 */
	public function setObject(string $object) : ApiComYopmailEmailMetadataInterface
	{
		$this->_object = $object;
		
		return $this;
	}
	
	/**
	 * Gets the object of this email.
	 * 
	 * @return string
	 */
	public function getObject() : string
	{
		return $this->_object;
	}
	
	/**
	 * Sets the date of reception of this email.
	 * 
	 * @param DateTimeInterface $dateReception
	 * @return ApiComYopmailEmailMetadataInterface
	 */
	public function setDateReception(DateTimeInterface $dateReception) : ApiComYopmailEmailMetadataInterface
	{
		$this->_dateReception = $dateReception;
		
		return $this;
	}
	
	/**
	 * Gets the date of reception of this email.
	 * 
	 * @return DateTimeInterface
	 */
	public function getDateReception() : DateTimeInterface
	{
		return $this->_dateReception;
	}
	
	/**
	 * Sets the headline of the email.
	 * 
	 * @param string $headline
	 * @return ApiComYopmailEmailMetadataInterface
	 */
	public function setHeadline(string $headline) : ApiComYopmailEmailMetadataInterface
	{
		$this->_headline = $headline;
		
		return $this;
	}
	
	/**
	 * Gets the headline of the email.
	 * 
	 * @return string
	 */
	public function getHeadline() : string
	{
		return $this->_headline;
	}
	
}
