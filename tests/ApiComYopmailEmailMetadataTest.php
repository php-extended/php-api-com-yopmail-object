<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-com-yopmail-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiComYopmail\Test;

use DateTimeImmutable;
use PhpExtended\ApiComYopmail\ApiComYopmailEmailMetadata;
use PhpExtended\Email\EmailAddressParser;
use PHPUnit\Framework\TestCase;

/**
 * ApiComYopmailEmailMetadataTest test file.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74TestMetadata
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiComYopmail\ApiComYopmailEmailMetadata
 * @internal
 * @small
 */
class ApiComYopmailEmailMetadataTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiComYopmailEmailMetadata
	 */
	protected ApiComYopmailEmailMetadata $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetAddress() : void
	{
		$this->assertEquals((new EmailAddressParser())->parse('test@example.com'), $this->_object->getAddress());
		$expected = (new EmailAddressParser())->parse('admin@example.com');
		$this->_object->setAddress($expected);
		$this->assertEquals($expected, $this->_object->getAddress());
	}
	
	public function testGetId() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getId());
		$expected = 'qsdfghjklm';
		$this->_object->setId($expected);
		$this->assertEquals($expected, $this->_object->getId());
	}
	
	public function testGetObject() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getObject());
		$expected = 'qsdfghjklm';
		$this->_object->setObject($expected);
		$this->assertEquals($expected, $this->_object->getObject());
	}
	
	public function testGetDateReception() : void
	{
		$this->assertEquals(DateTimeImmutable::createFromFormat('!Y-m-d H:i:s', '2000-01-01 00:00:01'), $this->_object->getDateReception());
		$expected = DateTimeImmutable::createFromFormat('!Y-m-d H:i:s', '2001-01-01 00:00:01');
		$this->_object->setDateReception($expected);
		$this->assertEquals($expected, $this->_object->getDateReception());
	}
	
	public function testGetHeadline() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getHeadline());
		$expected = 'qsdfghjklm';
		$this->_object->setHeadline($expected);
		$this->assertEquals($expected, $this->_object->getHeadline());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiComYopmailEmailMetadata((new EmailAddressParser())->parse('test@example.com'), 'azertyuiop', 'azertyuiop', DateTimeImmutable::createFromFormat('!Y-m-d H:i:s', '2000-01-01 00:00:01'), 'azertyuiop');
	}
	
}
